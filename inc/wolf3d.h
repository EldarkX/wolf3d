/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   wolf3d.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 20:26:08 by aantropo          #+#    #+#             */
/*   Updated: 2020/02/20 20:26:09 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef WOL3D_TEST_WOLF3D_H

# define WOL3D_TEST_WOLF3D_H

# include <SDL2/SDL.h>
# include <math.h>
# include <fcntl.h>
# include <pthread.h>

# include "vector3d.h"
# include "../libft/includes/libft.h"

# define ABS(x)		       (x) >= 0 ? (x) : -(x)
# define DROUND(d)      	ABS(d) < 0.000001 ? 0 : (d)

# define THREADS	    	4

# define WIDTH 		    	2000
# define HEIGHT 	    	1200

typedef struct s_wolf3d	    t_wolf3d;
typedef struct s_camera 	t_camera;

struct  					s_wolf3d
{
	SDL_Window			    *window;
	SDL_Renderer		    *renderer;
	SDL_Event		    	event;
	SDL_Surface 			*surface;
	int                     fd;
	char                    **map;
	t_camera                *camera;
};

struct                      s_camera
{
	t_vector3d              location;
	double                  fov;
	double                  pov;
};

t_wolf3d				    *ft_initialize_program();
t_wolf3d                    *ft_fill_wolf3d_struct(t_wolf3d *wolf3d, int fd);

void			    		ft_memory_clean(t_wolf3d *wolf3d);
void			    		ft_exit(t_wolf3d *wolf3d, int is_with_error,
							char *message);

void		    			ft_draw_loop(t_wolf3d *wolf3d);
void		    			ft_drawing(t_wolf3d *wolf3d);
void                       *ft_draw_call_on_thread(void *data);
void                        ft_put_pixel_to_surface(t_wolf3d *wolf3D,
								int x, int y, int color);

void			    		ft_switch_event(t_wolf3d *wolf3d);
void			    		ft_manage_key_input(t_wolf3d *wolf3d);

void                        ft_map_parser(t_wolf3d *wolf3d, char *file_name);
void                        ft_map_checker(t_wolf3d *wolf3d, int row, int col);
char                        **ft_get_map(t_wolf3d *wolf3d, int row, char *file_name);

void                        ft_raycasting(t_wolf3d *wolf3d, int x);

#endif
