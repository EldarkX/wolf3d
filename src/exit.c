/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 20:28:59 by aantropo          #+#    #+#             */
/*   Updated: 2020/02/20 20:29:00 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf3d.h"

void	ft_exit(t_wolf3d *wolf3d, int is_with_error, char *message)
{
	if (is_with_error)
		ft_printf("Error: %s\n", message);
	ft_memory_clean(wolf3d);
	exit(-1);
}

void	ft_memory_clean(t_wolf3d *wolf3d)
{
	free(wolf3d);
}