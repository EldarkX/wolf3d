/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input_handler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 21:14:44 by aantropo          #+#    #+#             */
/*   Updated: 2020/02/20 21:14:46 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf3d.h"

void	ft_switch_event(t_wolf3d *wolf3d)
{
	if (wolf3d->event.type == SDL_QUIT)
		ft_exit(wolf3d, 0, NULL);
	else if (wolf3d->event.type == SDL_KEYUP)
		ft_manage_key_input(wolf3d);
	else if (wolf3d->event.type == SDL_MOUSEMOTION)
	{
		wolf3d->camera->pov += wolf3d->event.motion.x / 80000.f;
	}
}

void	ft_manage_key_input(t_wolf3d *wolf3d)
{
	int key;

	key = wolf3d->event.key.keysym.sym;
	if (key == SDLK_ESCAPE)
		ft_exit(wolf3d, 0, NULL);
}