#include "../inc/wolf3d.h"

void     ft_map_parser(t_wolf3d *wolf3d, char *file_name)
{
	int     col;
	int     row;
	int     i;
	char    *line;

	col = 0;
	row = 0;
	while (get_next_line(wolf3d->fd, &line) > 0)
	{
		row++;
		if (!col)
			col = ft_strlen(line);
		else
		{
			if (col != ft_strlen(line) || !ft_strlen(line))
				ft_exit(wolf3d, 1, "Bad columns align.");
		}
		ft_strdel(&line);
	}
	ft_map_checker(wolf3d, row, col);
	wolf3d->map = ft_get_map(wolf3d, row, file_name);

	i = 0;

	/* check that map consists only 1 and 0, + bordered by 1 */
	while (i < row)
	{
		ft_printf("%s\n", wolf3d->map[i]);
		i++;
	}
}

void    ft_map_checker(t_wolf3d *wolf3d, int row, int col)
{
	if (col != row)
		ft_exit(wolf3d, 1, "Map must be squired.");
	if (!row)
		ft_exit(wolf3d, 1, "Empty map.");
	if (row < 3)
		ft_exit(wolf3d, 1, "Map must to have more than 2 lines.");
}

char    **ft_get_map(t_wolf3d *wolf3d, int row, char *file_name)
{
	char    **map;
	char    *line;
	int     i;

	if (close(wolf3d->fd) == -1)
		ft_exit(wolf3d, 1, "Can`t close file.");
	wolf3d->fd = open(file_name, O_RDONLY);
	if (wolf3d->fd < 0)
		ft_exit(wolf3d, 1, "Can`t open file.");
	map = (char **)malloc(sizeof(char *) * row);
	i = -1;
	while (get_next_line(wolf3d->fd, &line) > 0)
	{
		map[++i] = ft_strdup(line);
		ft_strdel(&line);
	}
	i = 0;

	/* check that map consists only 1 and 0, + bordered by 1 */
	while (i < row)
	{
		//ft_printf("%s\n", map[i]);
		i++;
	}
	return (map);
}