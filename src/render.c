/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   render.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 21:11:00 by aantropo          #+#    #+#             */
/*   Updated: 2020/02/20 21:11:05 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf3d.h"

void	ft_draw_loop(t_wolf3d *wolf3d)
{
    pthread_t threads[THREADS];
    int i;

    i = -1;
    while (++i < THREADS)
        pthread_create(&threads[i], NULL, ft_draw_call_on_thread, (void *)wolf3d);

	while (1)
	{
		if (SDL_PollEvent(&wolf3d->event))
			ft_switch_event(wolf3d);
		else
		{
			i = -1;
			while (++i < THREADS)
				pthread_join(threads[i], NULL);
			ft_draw_call_on_thread(wolf3d);
			ft_drawing(wolf3d);
		}
	}
}

void    *ft_draw_call_on_thread(void *data)
{
    static int  thread_number = 0;
    int         start_x;
    int         end_x;

    start_x = 0 + thread_number * (WIDTH / THREADS);
    if (thread_number == THREADS - 1)
        end_x = WIDTH;
    else
        end_x = start_x + (WIDTH / THREADS);

	while (start_x++ < end_x)
	{
		ft_raycasting((t_wolf3d *)data, start_x);
	}
	thread_number++;
	if (thread_number == THREADS)
		thread_number = 0;
    return (0);
}

void	ft_put_pixel_to_surface(t_wolf3d *wolf3d, int x, int y, int color)
{
    int index;

    SDL_LockSurface(wolf3d->surface);
    index = wolf3d->surface->pitch * y +
            x * wolf3d->surface->format->BytesPerPixel;
    *(unsigned *)(wolf3d->surface->pixels + index) = color;
    SDL_UnlockSurface(wolf3d->surface);
}

void	ft_drawing(t_wolf3d *wolf3d)
{
	SDL_Rect	src;
	SDL_Rect	dst;
	SDL_Texture	*texture;

	texture = SDL_CreateTextureFromSurface(wolf3d->renderer, wolf3d->surface);
	SDL_QueryTexture(texture, NULL, NULL, &src.w, &src.h);
	src.x = 0;
	src.y = 0;
	dst.x = src.x;
	dst.y = src.y;
	dst.w = src.w;
	dst.h = src.h;
	SDL_RenderClear(wolf3d->renderer);
	SDL_RenderCopy(wolf3d->renderer, texture, &src, &dst);
	SDL_RenderPresent(wolf3d->renderer);
	SDL_DestroyTexture(texture);
}