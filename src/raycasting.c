#include "../inc/wolf3d.h"

void    ft_raycasting(t_wolf3d *wolf3d, int x)
{
	double   rayAngle = (wolf3d->camera->pov - wolf3d->camera->fov / 2.)
			+ ((double)x / (double)WIDTH) * wolf3d->camera->fov;

	double  eyeX = sin(rayAngle);
	double  eyeY = cos(rayAngle);

	double  step;

	step = 0;

	while(1)
	{
		step += 0.01;
		int RayX = (int)(wolf3d->camera->location.x + eyeX * step);
		int RayY = (int)(wolf3d->camera->location.y + eyeY * step);

		if (wolf3d->map[RayX][RayY] == '1')
		{
			int nCeiling = (HEIGHT / 2.) -
					(HEIGHT / step);
			int nFloor = HEIGHT - nCeiling;
			for (int y = 0; y < HEIGHT; y++)
			{
				if (y <= nCeiling)
					ft_put_pixel_to_surface(wolf3d, x, y, 2147483647);
				else if(y > nCeiling && y <= nFloor)
					ft_put_pixel_to_surface(wolf3d, x, y, 2183647 * step);
				else
					ft_put_pixel_to_surface(wolf3d, x, y, 0);
			}
			return;
		}
	}
}