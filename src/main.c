/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 19:50:06 by aantropo          #+#    #+#             */
/*   Updated: 2020/02/20 19:50:10 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf3d.h"

int			main(int argc, char **argv)
{
	t_wolf3d    *wolf3d;
	int         fd;

	if (argc != 2)
	{
		ft_printf("usage:\n\t./wolf3d map_name\n");
		return (-1);
	}
	fd = open(argv[1], O_RDONLY);
	if (fd < 0)
		ft_exit(wolf3d, 1, "Can`t open file.");
	wolf3d = ft_initialize_program();
	ft_fill_wolf3d_struct(wolf3d, fd);
	ft_map_parser(wolf3d, argv[1]);
	if (close(fd) == -1)
	{
		ft_exit(wolf3d, 1, "Can`t close file.");
	}
	ft_draw_loop(wolf3d);
	return 0;
}

t_wolf3d	*ft_initialize_program()
{
	t_wolf3d *wolf3d;

	wolf3d = (t_wolf3d *)malloc(sizeof(t_wolf3d));
	if (SDL_Init(SDL_INIT_EVERYTHING) >= 0)
	{
		wolf3d->window = SDL_CreateWindow("wolf3d aantropo",
				SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, 0);
		if (!wolf3d->window)
			ft_exit(wolf3d, 1, "Could not create window.");
		wolf3d->renderer = SDL_CreateRenderer(wolf3d->window, -1,
											SDL_RENDERER_SOFTWARE);
		if (!wolf3d->renderer)
			ft_exit(wolf3d, 1, "Could not create renderer.");
		wolf3d->surface = SDL_CreateRGBSurface(0, WIDTH, HEIGHT, 32, 0, 0, 0, 0);
		if (!wolf3d->surface)
			ft_exit(wolf3d, 1, "Could not create surface.");
	}
	else
    {
	    ft_printf("%s", SDL_GetError());
        ft_exit(wolf3d, 1, "Could not init SDL.");
    }
	return (wolf3d);
}

t_wolf3d    *ft_fill_wolf3d_struct(t_wolf3d *wolf3d, int fd)
{
	wolf3d->fd = fd;
	wolf3d->camera = (t_camera *)malloc(sizeof(t_camera));
	wolf3d->camera->location = ft_new_vector(7, 7, 0);
	wolf3d->camera->fov = 3.14 / 3.;
	wolf3d->camera->pov = 0;
}
