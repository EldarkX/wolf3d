/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vector3d_operations_1.cpp                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aantropo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/20 19:50:18 by aantropo          #+#    #+#             */
/*   Updated: 2020/02/20 19:50:24 by aantropo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../inc/wolf3d.h"

t_vector3d		ft_new_vector(float x, float y, float z)
{
	t_vector3d vec;

	vec.x = x;
	vec.y = y;
	vec.z = z;
	return (vec);
}

t_vector3d		ft_add(t_vector3d v1, t_vector3d v2)
{
	t_vector3d v;

	v.x = v2.x + v1.x;
	v.y = v2.y + v1.y;
	v.z = v2.z + v1.z;
	return (v);
}

t_vector3d		ft_dif(t_vector3d v1, t_vector3d v2)
{
	t_vector3d v;

	v.x = v1.x - v2.x;
	v.y = v1.y - v2.y;
	v.z = v1.z - v2.z;
	return (v);
}

void			ft_print_vector(t_vector3d v, char *name)
{
	ft_printf("%s(%f,%f,%f)\n", name, v.x, v.y, v.z);
}